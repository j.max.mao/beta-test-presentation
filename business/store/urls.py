from django.urls import path

from .views import list_caps, show_caps, list_shirts, show_shirts, list_pants, show_pants




urlpatterns = [
    path("caps/", list_caps, name="list_caps"),
    path("caps/<int:pk>/", show_caps, name="show_caps"),
    path("shirts/", list_shirts, name="list_shirts"),
    path("shirts/<int:pk>/", show_shirts, name="show_shirts"),
    path("pants/", list_pants, name="list_pants"),
    path("pants/<int:pk>/", show_pants, name="show_pants"),
]