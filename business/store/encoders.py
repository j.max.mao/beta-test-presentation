from common.json import ModelEncoder
from .models import Caps, Shirts, Pants, Status



class CapsEncoder(ModelEncoder):
    models = Caps
    properties = [
        "name",
        "size",
        "fabric",
        "color",
        "style",
        "id"
    ]

class ShirtsEncoder(ModelEncoder):
    models = Shirts
    properties = [
        "name",
        "size",
        "fabric",
        "color",
        "style",
        "id"

    ]

class PantsEncoder(ModelEncoder):
    models = Pants
    properties = [
        "name",
        "size",
        "fabric",
        "color",
        "style",
        "id"
    ]

class StatusEncoder(ModelEncoder):
    models = Status
    properties = [
        "stat",
    ]