from django.db import models
from django.urls import reverse

# Create your models here.

class Caps(models.Model):
    name = models.CharField(max_length=200)
    size = models.SmallIntegerField()
    fabric = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    style = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("show_caps", kwargs={"pk": self.id})


class Shirts(models.Model):
    name = models.CharField(max_length=200)
    size = models.SmallIntegerField()
    fabric = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    style = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("show_shirts", kwargs={"pk": self.id})


class Pants(models.Model):
    name = models.CharField(max_length=200)
    size = models.SmallIntegerField()
    fabric = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    style = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("show_pants", kwargs={"pk": self.id})


class Status(models.Model):
    id = models.PositiveSmallIntegerField(primary_key=True)
    stat = models.CharField(max_length=10, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("id",)  # Default ordering for Status
        verbose_name_plural = "statuses"  # Fix the pluralization
