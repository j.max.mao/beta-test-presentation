from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Caps, Shirts, Pants
from .encoders import CapsEncoder, ShirtsEncoder, PantsEncoder
import json

# Create your views here.
@require_http_methods(["GET", "POST"])
def list_caps(request):
    if request.method == "GET":
        caps = Caps.objects.all()
        return JsonResponse(
            {"caps": caps},
            encoder = CapsEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            caps = Caps.objects.create(**content)
            return JsonResponse(
                caps,
                encoder=CapsEncoder,
                safe=False,
            )

        except Caps.DoesNotExist:
            response = JsonResponse({"message": "Invalid Address"})
            response.status_code = 400
            return response

@require_http_methods(['GET', 'DELETE', 'PUT'])
def show_caps(request, pk):
    if request.method == "GET":
        try:
            caps = Caps.objects.get(id=pk)
            return JsonResponse(
                caps,
                encoder=CapsEncoder,
                safe=False
            )
        except Caps.DoesNotExist:
            response = JsonResponse({"message": "It not here."})
            response.status_code = 404
            return response

    elif request.method == "DELETE":
        try:
            caps = Caps.objects.get(id=pk)
            caps.delete()
            return JsonResponse(
                caps,
                encoder=CapsEncoder,
                safe=False,
            )
        except Caps.DoesNotExist:
            return JsonResponse({"message": "This is not the code you're looking for."})
    
    else:
        try:
            content = json.loads(request.body)
            caps = Caps.objects.get(id=pk)

            props = ["name", "size", "fabric", "color", "style"]
            for prop in props:
                if prop in content:
                    setattr(caps, prop, content[prop])
            caps.save()

            return JsonResponse(
                caps,
                encoder=CapsEncoder,
                safe=False,
            )
            
        except Caps.DoesNotExist:
            response = JsonResponse({"message": "You're lost."})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def list_shirts(request):
    if request.method == "GET":
        shirts = Shirts.objects.all()
        return JsonResponse(
            {"shirts": shirts},
            encoder = ShirtsEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            shirts = Shirts.objects.create(**content)
            return JsonResponse(
                shirts,
                encoder=ShirtsEncoder,
                safe=False,
            )

        except Shirts.DoesNotExist:
            response = JsonResponse({"message": "Invalid Address"})
            response.status_code = 400
            return response


@require_http_methods(['GET', 'DELETE', 'PUT'])
def show_shirts(request, pk):
    if request.method == "GET":
        try:
            shirts = Shirts.objects.get(id=pk)
            return JsonResponse(
                shirts,
                encoder=ShirtsEncoder,
                safe=False
            )
        except Shirts.DoesNotExist:
            response = JsonResponse({"message": "It not here."})
            response.status_code = 404
            return response

    elif request.method == "DELETE":
        try:
            shirts = Shirts.objects.get(id=pk)
            shirts.delete()
            return JsonResponse(
                shirts,
                encoder=ShirtsEncoder,
                safe=False,
            )
        except Shirts.DoesNotExist:
            return JsonResponse({"message": "This is not the code you're looking for."})
    
    else:
        try:
            content = json.loads(request.body)
            shirts = Shirts.objects.get(id=pk)

            props = ["name", "size", "fabric", "color", "style"]
            for prop in props:
                if prop in content:
                    setattr(shirts, prop, content[prop])
            shirts.save()

            return JsonResponse(
                shirts,
                encoder=ShirtsEncoder,
                safe=False,
            )
            
        except Shirts.DoesNotExist:
            response = JsonResponse({"message": "You're lost."})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def list_pants(request):
    if request.method == "GET":
        pants = Pants.objects.all()
        return JsonResponse(
            {"pants": pants},
            encoder = PantsEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            pants = Pants.objects.create(**content)
            return JsonResponse(
                pants,
                encoder=PantsEncoder,
                safe=False,
            )

        except Pants.DoesNotExist:
            response = JsonResponse({"message": "Invalid Address"})
            response.status_code = 400
            return response


@require_http_methods(['GET', 'DELETE', 'PUT'])
def show_pants(request, pk):
    if request.method == "GET":
        try:
            pants = Pants.objects.get(id=pk)
            return JsonResponse(
                pants,
                encoder=PantsEncoder,
                safe=False
            )
        except Pants.DoesNotExist:
            response = JsonResponse({"message": "It not here."})
            response.status_code = 404
            return response

    elif request.method == "DELETE":
        try:
            pants = Pants.objects.get(id=pk)
            pants.delete()
            return JsonResponse(
                pants,
                encoder=PantsEncoder,
                safe=False,
            )
        except Pants.DoesNotExist:
            return JsonResponse({"message": "This is not the code you're looking for."})
    
    else:
        try:
            content = json.loads(request.body)
            pants = Pants.objects.get(id=pk)

            props = ["name", "size", "fabric", "color", "style"]
            for prop in props:
                if prop in content:
                    setattr(pants, prop, content[prop])
            pants.save()

            return JsonResponse(
                pants,
                encoder=PantsEncoder,
                safe=False,
            )
            
        except Pants.DoesNotExist:
            response = JsonResponse({"message": "You're lost."})
            response.status_code = 404
            return response

