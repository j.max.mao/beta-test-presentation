from .models import Movies, TvShows, Anime
from common.json import ModelEncoder



class MoviesEncoder(ModelEncoder):
    models = Movies
    properties = [
        "name",
        "description",
        "release_date",
        "img",
        "id"
    ]

class TvShowsEncoder(ModelEncoder):
    models = TvShows
    properties = [
        "name",
        "description",
        "release_date",
        "img",
        "id"
    ]

class AnimeEncoder(ModelEncoder):
    models = Anime
    properties = [
        "name",
        "description",
        "release_date",
        "img",
        "id"
    ]