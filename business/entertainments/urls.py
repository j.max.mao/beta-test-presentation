from django.urls import path

from .views import list_movies, show_movies, list_tvshows, show_tvshows, list_anime, show_anime




urlpatterns = [
    path("movies/", list_movies, name="list_movies"),
    path("movies/<int:pk>/", show_movies, name="show_movies"),
    path("tvshows/", list_tvshows, name="list_tvshows"),
    path("tvshows/<int:pk>/", show_tvshows, name="show_tvshows"),
    path("anime/", list_anime, name="list_anime"),
    path("anime/<int:pk>/", show_anime, name="show_anime"),
]