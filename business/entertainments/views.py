from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Movies, TvShows, Anime
from .encoders import MoviesEncoder, TvShowsEncoder, AnimeEncoder
import json

# Create your views here.


@require_http_methods(["GET", "POST"])
def list_movies(request):
    if request.method == "GET":
        movies = Movies.objects.all()
        return JsonResponse(
            {"movies": movies},
            encoder = MoviesEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            movies = Movies.objects.create(**content)
            return JsonResponse(
                movies,
                encoder=MoviesEncoder,
                safe=False,
            )

        except Movies.DoesNotExist:
            response = JsonResponse({"message": "Invalid Address"})
            response.status_code = 400
            return response


@require_http_methods(['GET', 'DELETE', 'PUT'])
def show_movies(request, pk):
    if request.method == "GET":
        try:
            movies = Movies.objects.get(id=pk)
            return JsonResponse(
                movies,
                encoder=MoviesEncoder,
                safe=False
            )
        except Movies.DoesNotExist:
            response = JsonResponse({"message": "It not here."})
            response.status_code = 404
            return response

    elif request.method == "DELETE":
        try:
            movies = Movies.objects.get(id=pk)
            movies.delete()
            return JsonResponse(
                movies,
                encoder=MoviesEncoder,
                safe=False,
            )
        except Movies.DoesNotExist:
            return JsonResponse({"message": "This is not the code you're looking for."})
    
    else:
        try:
            content = json.loads(request.body)
            movies = Movies.objects.get(id=pk)

            props = ["name", "description", "release_date", "img"]
            for prop in props:
                if prop in content:
                    setattr(movies, prop, content[prop])
            movies.save()

            return JsonResponse(
                movies,
                encoder=MoviesEncoder,
                safe=False,
            )
            
        except Movies.DoesNotExist:
            response = JsonResponse({"message": "You're lost."})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def list_tvshows(request):
    if request.method == "GET":
        tvshows = TvShows.objects.all()
        return JsonResponse(
            {"tvshows": tvshows},
            encoder = TvShowsEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            tvshows = TvShows.objects.create(**content)
            return JsonResponse(
                tvshows,
                encoder=TvShowsEncoder,
                safe=False,
            )

        except TvShows.DoesNotExist:
            response = JsonResponse({"message": "Invalid Address"})
            response.status_code = 400
            return response


@require_http_methods(['GET', 'DELETE', 'PUT'])
def show_tvshows(request, pk):
    if request.method == "GET":
        try:
            tvshows = TvShows.objects.get(id=pk)
            return JsonResponse(
                tvshows,
                encoder=TvShowsEncoder,
                safe=False
            )
        except TvShows.DoesNotExist:
            response = JsonResponse({"message": "It not here."})
            response.status_code = 404
            return response

    elif request.method == "DELETE":
        try:
            tvshows = TvShows.objects.get(id=pk)
            tvshows.delete()
            return JsonResponse(
                tvshows,
                encoder=TvShowsEncoder,
                safe=False,
            )
        except TvShows.DoesNotExist:
            return JsonResponse({"message": "This is not the code you're looking for."})
    
    else:
        try:
            content = json.loads(request.body)
            tvshows = TvShows.objects.get(id=pk)

            props = ["name", "description", "release_date", "img"]
            for prop in props:
                if prop in content:
                    setattr(tvshows, prop, content[prop])
            tvshows.save()

            return JsonResponse(
                tvshows,
                encoder=TvShowsEncoder,
                safe=False,
            )
            
        except TvShows.DoesNotExist:
            response = JsonResponse({"message": "You're lost."})
            response.status_code = 404
            return response

            
@require_http_methods(["GET", "POST"])
def list_anime(request):
    if request.method == "GET":
        anime = Anime.objects.all()
        return JsonResponse(
            {"anime": anime},
            encoder = AnimeEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            anime = Anime.objects.create(**content)
            return JsonResponse(
                anime,
                encoder=AnimeEncoder,
                safe=False,
            )

        except Anime.DoesNotExist:
            response = JsonResponse({"message": "Invalid Address"})
            response.status_code = 400
            return response


@require_http_methods(['GET', 'DELETE', 'PUT'])
def show_anime(request, pk):
    if request.method == "GET":
        try:
            anime = Anime.objects.get(id=pk)
            return JsonResponse(
                anime,
                encoder=AnimeEncoder,
                safe=False
            )
        except Anime.DoesNotExist:
            response = JsonResponse({"message": "It not here."})
            response.status_code = 404
            return response

    elif request.method == "DELETE":
        try:
            anime = Anime.objects.get(id=pk)
            anime.delete()
            return JsonResponse(
                anime,
                encoder=AnimeEncoder,
                safe=False,
            )
        except Anime.DoesNotExist:
            return JsonResponse({"message": "This is not the code you're looking for."})
    
    else:
        try:
            content = json.loads(request.body)
            anime = Anime.objects.get(id=pk)

            props = ["name", "description", "release_date", "img"]
            for prop in props:
                if prop in content:
                    setattr(anime, prop, content[prop])
            anime.save()

            return JsonResponse(
                anime,
                encoder=AnimeEncoder,
                safe=False,
            )
            
        except Anime.DoesNotExist:
            response = JsonResponse({"message": "You're lost."})
            response.status_code = 404
            return response