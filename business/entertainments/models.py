from django.db import models
from django.urls import reverse

# Create your models here.

class Movies(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField(max_length=400)
    release_date = models.DateField()
    img = models.URLField()

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("show_movies", kwargs={"pk": self.pk})


class TvShows(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField(max_length=400)
    release_date = models.DateField()
    img = models.URLField()

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("show_tvshows", kwargs={"pk": self.pk})


class Anime(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField(max_length=400)
    release_date = models.DateField()
    img = models.URLField()

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("show_anime", kwargs={"pk": self.pk})