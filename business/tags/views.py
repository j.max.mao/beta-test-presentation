from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Tag
from .encoders import TagEncoder
import json


# Create your views here.
@require_http_methods(["GET", "POST"])
def list_caps(request):
    if request.method == "GET":
        tag = Tag.objects.all()
        return JsonResponse(
            {"tag": tag},
            encoder = TagEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            tag = Tag.objects.create(**content)
            return JsonResponse(
                tag,
                encoder=TagEncoder,
                safe=False,
            )

        except Tag.DoesNotExist:
            response = JsonResponse({"message": "Invalid Address"})
            response.status_code = 400
            return response

@require_http_methods(['GET', 'DELETE', 'PUT'])
def show_tag(request, pk):
    if request.method == "GET":
        try:
            tag = Tag.objects.get(id=pk)
            return JsonResponse(
                tag,
                encoder=TagEncoder,
                safe=False
            )
        except Tag.DoesNotExist:
            response = JsonResponse({"message": "It not here."})
            response.status_code = 404
            return response

    elif request.method == "DELETE":
        try:
            tag = Tag.objects.get(id=pk)
            tag.delete()
            return JsonResponse(
                tag,
                encoder=TagEncoder,
                safe=False,
            )
        except Tag.DoesNotExist:
            return JsonResponse({"message": "This is not the code you're looking for."})
    
    else:
        try:
            content = json.loads(request.body)
            tag = Tag.objects.get(id=pk)

            props = ["name"]
            for prop in props:
                if prop in content:
                    setattr(tag, prop, content[prop])
            tag.save()

            return JsonResponse(
                tag,
                encoder=TagEncoder,
                safe=False,
            )
            
        except Tag.DoesNotExist:
            response = JsonResponse({"message": "You're lost."})
            response.status_code = 404
            return response
