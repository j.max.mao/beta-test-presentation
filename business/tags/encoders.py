from common.json import ModelEncoder
from .models import Tag, Ratings



class TagEncoder(ModelEncoder):
    models = Tag
    properties = [
        "name",
    ]

class RatingsEncoder(ModelEncoder):
    models = Ratings
    properties = [
        "value"
    ]
