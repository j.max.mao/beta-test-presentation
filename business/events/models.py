from django.db import models
from django.urls import reverse

# Create your models here.


class Locations(models.Model):
    name = models.CharField(max_length=200)
    city = models.CharField(max_length=200)
    picture_url = models.URLField(null=True)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("show_locations", kwargs={"pk": self.id})

    class Meta:
        ordering = ("name",) 


class Conventions(models.Model):
    name = models.CharField(max_length=200)
    starts = models.DateTimeField()
    ends = models.DateTimeField()

    location = models.ForeignKey(
        Locations,
        related_name="conventions",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("show_conventions", kwargs={"pk": self.id})