from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Locations, Conventions
from .encoders import LocationsEncoder, ConventionsEncoder
import json


# Create your views here.


@require_http_methods(["GET", "POST"])
def list_locations(request):
    if request.method == "GET":
        locations = Locations.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder = LocationsEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            locations = Locations.objects.create(**content)
            return JsonResponse(
                locations,
                encoder=LocationsEncoder,
                safe=False,
            )

        except Locations.DoesNotExist:
            response = JsonResponse({"message": "Invalid Address"})
            response.status_code = 400
            return response


@require_http_methods(['GET', 'DELETE', 'PUT'])
def show_locations(request, pk):
    if request.method == "GET":
        try:
            locations = Locations.objects.get(id=pk)
            return JsonResponse(
                locations,
                encoder=LocationsEncoder,
                safe=False
            )
        except Locations.DoesNotExist:
            response = JsonResponse({"message": "It not here."})
            response.status_code = 404
            return response

    elif request.method == "DELETE":
        try:
            locations = Locations.objects.get(id=pk)
            locations.delete()
            return JsonResponse(
                locations,
                encoder=LocationsEncoder,
                safe=False,
            )
        except Locations.DoesNotExist:
            return JsonResponse({"message": "This is not the code you're looking for."})
    
    else:
        try:
            content = json.loads(request.body)
            locations = Locations.objects.get(id=pk)

            props = ["name", "starts", "ends", "locations"]
            for prop in props:
                if prop in content:
                    setattr(locations, prop, content[prop])
            locations.save()

            return JsonResponse(
                locations,
                encoder=LocationsEncoder,
                safe=False,
            )
            
        except Locations.DoesNotExist:
            response = JsonResponse({"message": "You're lost."})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def list_conventions(request):
    if request.method == "GET":
        conventions = Conventions.objects.all()
        return JsonResponse(
            {"conventions": conventions},
            encoder = ConventionsEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            conventions = Conventions.objects.create(**content)
            return JsonResponse(
                conventions,
                encoder=ConventionsEncoder,
                safe=False,
            )

        except Conventions.DoesNotExist:
            response = JsonResponse({"message": "Invalid Address"})
            response.status_code = 400
            return response


@require_http_methods(['GET', 'DELETE', 'PUT'])
def show_conventions(request, pk):
    if request.method == "GET":
        try:
            conventions = Conventions.objects.get(id=pk)
            return JsonResponse(
                conventions,
                encoder=ConventionsEncoder,
                safe=False
            )
        except Conventions.DoesNotExist:
            response = JsonResponse({"message": "It not here."})
            response.status_code = 404
            return response

    elif request.method == "DELETE":
        try:
            conventions = Conventions.objects.get(id=pk)
            conventions.delete()
            return JsonResponse(
                conventions,
                encoder=ConventionsEncoder,
                safe=False,
            )
        except Conventions.DoesNotExist:
            return JsonResponse({"message": "This is not the code you're looking for."})
    
    else:
        try:
            content = json.loads(request.body)
            conventions = Conventions.objects.get(id=pk)

            props = ["name", "starts", "ends", "locations"]
            for prop in props:
                if prop in content:
                    setattr(conventions, prop, content[prop])
            conventions.save()

            return JsonResponse(
                conventions,
                encoder=conventions,
                safe=False,
            )
            
        except Conventions.DoesNotExist:
            response = JsonResponse({"message": "You're lost."})
            response.status_code = 404
            return response
