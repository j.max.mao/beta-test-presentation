from common.json import ModelEncoder
from .models import Locations, Conventions



class LocationsEncoder(ModelEncoder):
    models = Locations
    properties = [
        "name",
        "city",
        "picture_url",
        "id"
    ]

class ConventionsEncoder(ModelEncoder):
    models = Conventions
    properties = [
        "name",
        "starts",
        "ends",
        "locations",
        "id"
    ]
    
    encoders = {
        "locations": LocationsEncoder(),
    }