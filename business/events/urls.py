from django.urls import path

from .views import list_locations, show_locations, list_conventions, show_conventions




urlpatterns = [
    path("locations/", list_locations, name="list_locations"),
    path("locations/<int:pk>/", show_locations, name="show_locations"),
    path("conventions/", list_conventions, name="list_conventions"),
    path("conventions/<int:pk>/", show_conventions, name="show_conventions"),
]