from common.json import ModelEncoder
from .models import AccountVO, Attendees



class AttendeesEncoder(ModelEncoder):
    models = Attendees
    properties = [
        "name",
        "email",
        "created",
        "id"
    ]

class EventsVOEncoder(ModelEncoder):
    model = AccountVO
    properties = [
        "name",
        "import_href"
    ]