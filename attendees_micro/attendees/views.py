from django.http import JsonResponse
import json
from django.views.decorators.http import require_http_methods
from .models import Attendees, EventsVO
from .encoders import (
    AttendeesEncoder
)

# Create your views here.


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, events_vo_id=None):
    if request.method == "GET":
        if events_vo_id is not None:
            attendees = Attendees.objects.filter(events=events_vo_id)
        else:
            attendees = Attendees.objects.all()
        return JsonResponse(
            {"attendees": attendees},
            encoder=AttendeesEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            events_href = content["events"]
            events = EventsVO.objects.get(import_href=events_href)
            content["events"] = events

        except EventsVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid events id"},
                status=400,
            )

        attendee = Attendees.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeesEncoder,
            safe=False,
        )


@require_http_methods(['GET', 'DELETE', 'PUT'])
def api_show_attendees(request, pk):
    if request.method == "GET":
        try:
            attendees = Attendees.objects.get(id=pk)
            return JsonResponse(
                attendees,
                encoder=AttendeesEncoder,
                safe=False
            )
        except Attendees.DoesNotExist:
            response = JsonResponse({"message": "Who's is this?"})
            response.status_code = 404
            return response

    elif request.method == "DELETE":
        try:
            attendees = Attendees.objects.get(id=pk)
            attendees.delete()
            return JsonResponse(
                attendees,
                encoder=AttendeesEncoder,
                safe=False,
            )

        except Attendees.DoesNotExist:
            return JsonResponse({"message": "You're lost."})

    else:
        try:
            content = json.loads(request.body)
            attendees = Attendees.objects.get(id=pk)

            props = ["name", "email"]
            for prop in props:
                if prop in content:
                    setattr(attendees, prop, content[prop])
            attendees.save()

            return JsonResponse(
                attendees, 
                encoder=AttendeesEncoder,
                safe = False,
            )

        except Attendees.DoesNotExist:
            response = JsonResponse({"message": "Look over there!"})
            response.status_code = 404
            return response
