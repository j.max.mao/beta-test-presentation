from django.urls import path

from .views import api_list_attendees, api_show_attendees


urlpatterns = [
    path("attendees/", api_list_attendees, name="api_create_attendees"),
    path("attendees/<int:pk>/", api_show_attendees, name="api_show_attendee"),
    path(
        "events/<int:events_vo_id>/attendees/",
        api_list_attendees,
        name="api_list_attendees",
    ),
]