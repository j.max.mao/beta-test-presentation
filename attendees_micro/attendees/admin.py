from django.contrib import admin

from .models import Attendees, Badge


@admin.register(Attendees)
class AttendeeAdmin(admin.ModelAdmin):
    pass


@admin.register(Badge)
class BadgeAdmin(admin.ModelAdmin):
    pass

