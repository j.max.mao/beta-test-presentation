from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from django.urls import reverse

# Create your models here.

class AccountVO(models.Model):
    email = models.EmailField(unique=True)
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)


class EventsVO(models.Model):
    name = models.CharField(max_length=200)
    import_href = models.CharField(max_length=200, unique=True)


class Attendees(models.Model):
    name = models.CharField(max_length=200)
    email = models.EmailField()
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

    def create_badge(self):
        try:
            self.badge
        except ObjectDoesNotExist:
            Badge.objects.create(attendee=self)

    def get_api_url(self):
        return reverse("api_show_attendees", kwargs={"pk": self.pk})


class Badge(models.Model):
    created = models.DateTimeField(auto_now_add=True)

    attendee = models.OneToOneField(
        Attendees,
        related_name="badge",
        on_delete=models.CASCADE,
        primary_key=True,
    )